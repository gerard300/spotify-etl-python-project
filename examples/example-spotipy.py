import spotipy
import os
from spotipy.oauth2 import SpotifyOAuth

os.environ['SPOTIPY_CLIENT_ID'] = "842b845f65ce4a6fa5b01ce63d58f824"
os.environ['SPOTIPY_CLIENT_SECRET'] = "5c5d60ca98b14d97a912fceae543636d"
os.environ['SPOTIPY_REDIRECT_URI'] = "https://etl-spotify-python-app"

scope = "user-read-recently-played"

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope))

results = sp.current_user_saved_tracks()
for idx, item in enumerate(results['items']):
    track = item['track']
    print(idx, track['artists'][0]['name'], " – ", track['name'])