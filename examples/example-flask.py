# import main Flask class and request object
from flask import Flask, request

# Example : https://etl-spotify-python-app/?code=AQCnSV7lHmjhiXNgfvnXSydXWGNdEvzt6n3pmXkl5AEbfSCZALhCExR-roVolibhPdpB5-kwYPYPlgNqw3F9Ger4pVem_YaqfFnbbx8vyGVXcaUdYzDX0Ez5-YMQjOCaFDVmshOHJuNR7bScYc988bU3gPPeRUdjtA1cprkieZCO9e8NpHv2qGXAfu_O1QgFpGFk8I8

# My flask server http://127.0.0.1:8080
# create the Flask app
app = Flask(__name__)


@app.route('/etl-spotify-python-app')
def query_example():
    # if key doesn't exist, returns None
    code = request.args.get('code')
    return '''<h1>New Code has been generated: {}</h1>'''.format(code)


if __name__ == '__main__':
    # run app in debug mode on port 8080
    app.run(debug=True, port=8080)
