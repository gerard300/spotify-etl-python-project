# Full ETL process with Python

# Extract

This project uses Spotify API to gather information about the last 50 played songs

JSON information about tracks: 
```text
    * duration_ms - The duration of the track in milliseconds.
    * key - The estimated overall key of the track. Integers map to pitches using standard Pitch Class notation .
     E.g. 0 = C, 1 = C♯/D♭, 2 = D, and so on. If no key was detected, the value is -1.
    * mode - Mode indicates the modality (major or minor) of a track, the type of scale from which 
      its melodic content is derived. Major is represented by 1 and minor is 0.
    * time_signature - An estimated overall time signature of a track. The time signature (meter) is 
      a notational convention to specify how many beats are in each bar (or measure).
    * acousticness - A confidence measure from 0.0 to 1.0 of whether the track is acoustic.
      1.0 represents high confidence the track is acoustic. The distribution of values for this feature
      look like this: Acousticness distribution
    * danceability - Danceability describes how suitable a track is for dancing based on a combination
      of musical elements including tempo, rhythm stability, beat strength, and overall regularity.
      A value of 0.0 is least danceable and 1.0 is most danceable. The distribution of values for this
      feature look like this: Danceability distribution
    * energy - Energy is a measure from 0.0 to 1.0 and represents a perceptual measure of intensity and activity.
      Typically, energetic tracks feel fast, loud, and noisy. For example, death metal has high energy,
      while a Bach prelude scores low on the scale. Perceptual features contributing to this attribute include dynamic
      range, perceived loudness, timbre, onset rate, and general entropy.
      The distribution of values for this feature look like this: Energy distribution
    * instrumentalness - Predicts whether a track contains no vocals. “Ooh” and “aah” sounds are treated
      as instrumental in this context. Rap or spoken word tracks are clearly “vocal”.
      The closer the instrumentalness value is to 1.0, the greater likelihood the track contains no vocal content.
      Values above 0.5 are intended to represent instrumental tracks, but confidence is higher as the value approaches
      1.0. The distribution of values for this feature look like this: Instrumentalness distribution
    * liveness - Detects the presence of an audience in the recording. Higher liveness values represent an
      increased probability that the track was performed live. A value above 0.8 provides strong likelihood
      that the track is live. The distribution of values for this feature look like this: Liveness distribution
    * loudness - The overall loudness of a track in decibels (dB). Loudness values are averaged across
      the entire track and are useful for comparing relative loudness of tracks. Loudness is the quality of
      a sound that is the primary psychological correlate of physical strength (amplitude).
      Values typical range between -60 and 0 db. The distribution of values for this feature
      look like this: Loudness distribution
    * speechiness - Speechiness detects the presence of spoken words in a track. The more exclusively speech-like
      the recording (e.g. talk show, audio book, poetry), the closer to 1.0 the attribute value. Values above 0.66
      describe tracks that are probably made entirely of spoken words. Values between 0.33 and 0.66 describe tracks
      that may contain both music and speech, either in sections or layered, including such cases as rap music.
      Values below 0.33 most likely represent music and other non-speech-like tracks. The distribution of values for
      this feature look like this: Speechiness distribution
    * valence - A measure from 0.0 to 1.0 describing the musical positiveness conveyed by a track.
      Tracks with high valence sound more positive (e.g. happy, cheerful, euphoric), while tracks with low valence
      sound more negative (e.g. sad, depressed, angry). The distribution of values for this feature
      look like this: Valence distribution
    * tempo - The overall estimated tempo of a track in beats per minute (BPM). In musical terminology, tempo is
      the speed or pace of a given piece and derives directly from the average beat duration. The distribution of
      values for this feature look like this: Tempo distribution
    * id - The Spotify ID for the track.
    * type - The object type: “audio_features”
    * popularity - The popularity of the track. The value will be between 0 and 100, with 100 being the most popular.
      The popularity is calculated by algorithm and is based, in the most part, on the total number of plays the track
      has had and how recent those plays are. Generally speaking, songs that are being played a lot now will have a
      higher popularity than songs that were played a lot in the past. Artist and album popularity is derived
      mathematically from track popularity. Note that the popularity value may lag actual popularity by
      a few days: the value is not updated in real time.
```

# Transform 

# Load

## About ORM

Prerequisites for this project:
```shell
pipenv install sqlalchemy psycopg2

docker run --name full-etl-postgresDB \
    -e POSTGRES_PASSWORD=P4ssw0rdT3mp \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_DB=exampledb \
    -d -p 5432:5432 postgres
```
Notes:
```text
Example connection: dialect+driver://username:password@host:port/database

Notes:
    * dialect : supported dialect by SQLAlchemy
    * driver : It's actually the DBAPI
```

First, we will learn about some core concepts of SQLAlchemy
(like engines and connection pools), then we will learn how to
map Python classes and its relationships to database tables,
and finally we will learn how to retrieve (query) data from these tables

### SQLAlchemy Introduction
SQLAlchemy is a library that manage the communication between Python
programs and databases. Most of the time, this library is used as an Object Relational Mapper (ORM)
tool that translates Python classes to tables on relational databases and
automatically converts function calls to SQL statements.
SQLAlchemy provides a standard interface that allows developers to create database-agnostic code
to communicate with a wide variety of database engines.

###  Main concepts
 There are some concepts that every Python developer needs to
 understand before dealing with SQLAlchemy applications:

* **Python DBAPI**: (an acronym for DataBase API) how Python modules that integrate with databases should 
    expose their interfaces. We are not going to interact with this API directly (We will use SQLAlchemy
    instead)
* **SQLAlchemy Engines**: Whenever we want to use SQLAlchemy to interact with a database,
    we need to create an Engine. Engines, on SQLAlchemy, are used to manage two crucial
    factors: **Pools** and **Dialects**. SQLAlchemy uses them to interact with **DBAPI**
* **SQLAlchemy Connection Pools**: Connection pooling is one of the most traditional implementations of the object pool pattern.
    Instead of spending time to create objects that are frequently needed (like connections to databases) the program
    fetches an existing object from the pool, uses it as desired, and puts back when done.
    The main reason why programs take advantage of this design pattern is to improve performance. In the case of database connections,
    opening and maintaining new ones is expensive, time-consuming, and wastes resources. This object allows easier 
    manage the number of connections that an application might use simultaneously.
    creating an Engine through the `create_engine()` function usually generates a QueuePool. This kind of pool comes configured with some reasonable defaults, like a maximum pool size of 5 connections.
    But for production programs we need to fine-tune pools to our project needs. There are a set of configuration options we have to know:
    - **pool_size**: number of connections that pool can handle
    - **max_overflow**: specifies how many exceeding connections pool supports
    - **pool_recycle**: configure the max duration of connections in the pool
    - **pool_timeout**: define how many seconds the program have to wait before getting a connection from the pool
* **SQLAlchemy Dialects**: SQL variations from the different databases. SQLAlchemy needs to be aware of the type of databases that is connecting to
    SQLAlchemy have the following list of dialects:
    - Firebird
    - Microsoft SQL Server
    - MySQL
    - Oracle
    - PostgreSQL
    - SQLite
    - Sybase


```python
from sqlalchemy import create_engine
engine = create_engine('postgresql://usr:pass@localhost:5432/sqlalchemy')
```
his process is postponed to when it's needed
(like when we submit a query, or when create/update a row in a table).

## PostgreSQL basic commands 

¿How to access PostgreSQL by connecting to the container?



```shell
sudo docker exec -it full-etl-postgresDB /bin/bash
psql -U postgres
\c exampledb
\dt
SELECT * FROM table_songs;
```