import requests
import logging
import sys
from datetime import datetime, timedelta
from utils import generate_token
from utils import retrieve_tracks
from utils import record_processing
from orm.song import Song, Session

# Some constants
LIMIT = 50  # The maximum number of items to return. Minimum 1 , Maximum 50
TOKEN = generate_token.generate()

# def check_if_valid_data(df: pd.DataFrame) -> bool:
#     # First: check if your DataFrame is empty
#     if df.empty:
#         print('No songs downloaded, finishing execution...')
#     # Second: check if your primary key is unique
#     if not pd['played_timestamp'].is_unique:
#         raise Exception('The primary key rule has been violated. Shutting down')
#     # Third: check nulls
#     if pd.isnull().values.any():
#         raise Exception('Null value found')
#     # Fourth: check if all timestamps are from yesterday's day
#     yesterday = datetime.now() - timedelta(1)
#     yesterday = yesterday.replace(hour=0, minute=0, second=0, microsecond=0)  # create the proper DateTime
#     for value in df['played_timestamp'].tolist():
#         # played_timestamp string format = "2016-12-13T20:42:17.016Z" --> "%Y-%m-%dT%H:%M:%S"
#         if parser.parse(value).replace(hour=0, minute=0, second=0, microsecond=0) != yesterday:
#             raise Exception('At least one of the returned songs does not come from within the last 24h')


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    logger = logging.getLogger()
    # https://developer.spotify.com/documentation/web-api/reference/#object-audiofeaturesobject
    # https://developer.spotify.com/documentation/web-api/reference/#endpoint-get-audio-features

    header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {TOKEN}"
    }
    # data_tracks is a list of tracks, each track is a dictionary with the song information !
    data_tracks = retrieve_tracks.retrieve_tracks(header)
    db_session = Session()

    for record in data_tracks:
        new_song = record_processing.process(header,record)
        db_session.add(new_song)
        logger.info(f"New record detected [{new_song.song_name},{new_song.artists_names},{new_song.played_timestamp},"
                    f"{new_song.energy}{new_song.tempo},{new_song.valence},{new_song.speechiness},"
                    f"{new_song.loudness},{new_song.liveness},{new_song.instrumentalness},"
                    f"{new_song.acousticness},{new_song.danceability}")
    db_session.commit()
    db_session.close()
