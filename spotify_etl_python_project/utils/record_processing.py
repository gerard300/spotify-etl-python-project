from utils import retrieve_tracks
from orm.song import Song

def process(header,record):
        data_track = retrieve_tracks.retrieve_info_track(header, record)
        song_name = record['track']['name']
        artists = [record['track']['artists'][i]['name'] for i in range(len(record['track']['artists']))]
        played_at = record['played_at']
        energy_score = data_track['energy']
        tempo = data_track['tempo']
        valence = data_track['valence']
        speechiness_score = data_track['speechiness']
        loudness = data_track['loudness']
        liveness = data_track['liveness']
        instrumentalness = data_track['instrumentalness']
        acousticness = data_track['acousticness']
        danceability = data_track['danceability']
        
        return Song(song_name, artists, played_at, energy_score, tempo,
                                valence, speechiness_score, loudness, liveness, instrumentalness,
                                acousticness, danceability)