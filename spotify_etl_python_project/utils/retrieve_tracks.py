from datetime import datetime, timedelta
import requests


def retrieve_tracks(header: dict) -> dict:
    today = datetime.now()
    yesterday = today - timedelta(1)  # Time delta gives you the duration of 1 day in this case
    yesterday = yesterday.replace(hour=0, minute=0, second=0, microsecond=0)  # Create a proper yesterday date
    # logger.info(f'Yesterday day is : {yesterday.strftime("%Y-%m-%dT%H:%M:%S")}')
    yesterday_unix_time = int(yesterday.timestamp() * 1000)  # *1000 because it returns in seconds
    response_tracks = requests.get(
        f"https://api.spotify.com/v1/me/player/recently-played?limit={50}&after={yesterday_unix_time}",
        headers=header)

    return response_tracks.json()['items']


def retrieve_info_track(header: dict, record: dict):
    return requests.get(
        f"https://api.spotify.com/v1/audio-features/{record['track']['id']}",
        headers=header).json()
