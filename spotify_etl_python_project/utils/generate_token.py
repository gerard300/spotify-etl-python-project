from spotipy.oauth2 import SpotifyOAuth
from utils.app_sensitive_info import client_id, client_secret, redirect_uri
from pathlib import Path


def generate() -> str:
    scope = 'user-read-recently-played'
    cache = Path("")
    username = 'gerard300'
    oauth = SpotifyOAuth(client_id, client_secret, redirect_uri, scope=scope)
    dict_token = oauth.get_access_token()
    token = dict_token['access_token']
    return token
